# Clean compilation
make clean

# Extract strings to translate
make gettext

# Update strings (ES)
sphinx-intl update -l es

# Compile the documentation (ES)
make -e SPHINXOPTS="-D language='es'" html
