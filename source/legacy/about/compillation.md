# Compilation of documentation
The Qt programming course has evolved over time. Initially it started as a course focused on Ubuntu Touch and little by little it has been opened to desktop applications. To make it more affordable, among other reasons, I will modify some things of the course.

The changes affect both the tool that uses the course, which will become Sphinx and the internal organization of the course. In the current chapters it is assumed that the user uses the programming environment of the Ubuntu SDK. This SDK is no longer available and there are more modern alternatives that solve many of the current problems. Initially I will take the desktop as a base environment, and I will make the appropriate comments for the parts that are exclusive to Ubuntu Touch.

Although it may seem so, the changes are not so radical. The code and logic that is followed in the different chapters is practically the same. Only some elements related to Qt will change.

Although I will be working on the course throughout the month, I will upload the documentation on the last Friday of each month. At that time I will compile the documentation and upload it to the InnerZaurus website.

## Installation

The instructions apply to distributions derived from Ubuntu. The packages that have to be installed are:

* build-essential
* Sphinx and Python 3 files

Commands:

```bash
sudo apt-get install build-essential
sudo apt-get install python3-sphinx sphinxsearch python3-pip
```

Extensions for Sphinx:

```bash
pip3 install sphinx-intl
pip3 install --upgrade recommonmark
pip3 install sphinx_rtd_theme
```

If you want run de compilation (English):

```bash
make html
```

To compile a translation (Spanish):
```bash
make gettext
sphinx-intl update -p build/gettext -l es
make -e SPHINXOPTS="-D language='es'" html
```
